#!/usr/bin/env python3
import unittest
import lineager


class LineagerTestCase(unittest.TestCase):

    def test_get_lineage(self):
        """For a given species, return lineage data as a dictionary."""
        organism = 'Escherichia coli'
        expected_lineage = {
            "Escherichia coli": "cellular organisms;Bacteria;"
                                "Proteobacteria;Gammaproteobacteria;"
                                "Enterobacterales;Enterobacteriaceae;"
                                "Escherichia;Escherichia coli"
        }
        lineage = lineager.get_lineage(organism=organism)
        self.assertDictEqual(lineage, expected_lineage)

    def test_no_lineage(self):
        """If an organism's lineage can't be obtained, return an empty
        dictionary.

        """
        organism = "invalid"
        lineage = lineager.get_lineage(organism=organism)
        self.assertFalse(lineage)

    def test_invalid_name(self):
        """Handly None or empty organism name."""
        with self.assertRaises(ValueError):
            lineager.get_lineage(organism="")

        with self.assertRaises(AttributeError):
            lineager.get_lineage(organism=None)

    # def test_get_file_lineage(self):
    #     """Given an input file with species names one per line, generate and
    #     output csv file with lineage for all valid species names.
    #
    #     """
    #     pass



if __name__ == '__main__':
    unittest.main(verbosity=3)
