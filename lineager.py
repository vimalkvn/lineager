#!/usr/bin/env python3
import sys
import logging
import argparse
import MySQLdb

# configure logging
logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)-15s %(levelname)-8s %(message)s",
    datefmt="%Y-%m-%d %H:%M")


def get_lineage(organism=None):
    """Given an species' name, returns lineage information as a dictionary.

    For the database connection to work in MySQLdb.connect(), create file
    ~/.my.cnf with the following content

    [client]
    user = biovm
    password = password-here
    database = biosql

    """
    message = "Organism name is not valid"
    try:
        organism = organism.strip()
    except AttributeError:
        logging.warning("{}: {}".format(message, organism))
        raise

    if not len(organism):
        raise ValueError("{}: {}".format(message, organism))

    # create connection, cursor
    try:
        connection = MySQLdb.connect(read_default_file="~/.my.cnf")
    except MySQLdb.connections.OperationalError:
        logging.error(
            "Failed to connect to database using the provided ~/.my.cnf "
            "file. Please check if the user, password and database "
            "names are correct.\n")
        raise

    cursor = connection.cursor()
    sql = """
    SELECT s.taxon_id, s.ncbi_taxon_id, s.parent_taxon_id,
    s.node_rank, t.name FROM taxon s JOIN taxon_name t ON
    s.taxon_id = t.taxon_id WHERE t.name_class = 'scientific name' 
    AND t.name=%s;
    """
    params = (organism, )
    sql_data = exec_sql_get_data(cursor, sql, params)
    if not sql_data:
        logging.warning("No lineage data for organism: {}".format(organism))
        cursor.close()
        connection.close()
        return {}

    lineage_data = [sql_data["name"]]
    parent_taxon_id = sql_data["parent_taxon_id"]
    while parent_taxon_id > 1:
        sql = """
        SELECT s.parent_taxon_id, s.node_rank, t.name FROM taxon s 
        JOIN taxon_name t ON s.taxon_id=t.taxon_id 
        WHERE t.name_class='scientific name' AND s.taxon_id=%s;
        """
        params = (parent_taxon_id, )
        sql_data = exec_sql_get_data(cursor, sql, params)
        lineage_data.append(sql_data["name"])
        parent_taxon_id = sql_data["parent_taxon_id"]
    lineage_data.reverse()
    return {organism: ";".join(lineage_data)}


def exec_sql_get_data(cursor, sql, params):
    """Execute the given sql statement with params and return data as a
    dictionary of column names and values.

    """
    cursor.execute(sql, params)
    column_names = [i[0] for i in cursor.description]
    row = cursor.fetchone()

    if not cursor.rowcount:
        # No rows for the given SQL statement
        return {}

    sql_data = dict(zip(column_names, row))
    return sql_data


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        prog="lineager",
        description="Get lineage of organisms from NCBI taxonomy data")

    parser.add_argument(
        "-n", help="Name of the organism", dest="name", nargs='+')
    group = parser.add_argument_group(
        title="options when an input file is provided")
    group.add_argument(
        "-f", help="File containing names of organisms. One per line",
        dest="input")
    group.add_argument(
        "-o", help="File to save output to. Default: %(default)s",
        dest="output", default="lineage.csv")

    args = parser.parse_args()
    if not (args.name or args.input):
        sys.exit(parser.print_help())

    if args.name:
        name = " ".join(args.name)
        logging.info("Processing organism name provided at the "
                     "command line: {}\n".format(name))
        lineage = get_lineage(name)
        if lineage:
            print("Organism,Lineage")
            for k, v in lineage.items():
                print("{},{}\n".format(k, v))

    if args.input:
        logging.info("Processing organism names from input file: "
                     "{}".format(args.input))
        with open(args.output, "w") as output:
            output.write("Organism,Lineage\n")
            for line in open(args.input):
                name = line.strip()
                try:
                    lineage = get_lineage(name)
                except (ValueError, AttributeError):
                    continue
                if lineage:
                    for k, v in lineage.items():
                        output.write("{},{}\n".format(k, v))
            logging.info("Output written to {}".format(args.output))
